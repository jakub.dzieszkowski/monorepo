/// A class stub implementation to support conditional imports.
class Renderer {
  /// Creates a new instance of the [Renderer].
  const Renderer();

  /// Indicates whether this application's instance uses the SKIA renderer.
  bool get isSkia => true;
}
